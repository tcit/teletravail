[[_TOC_]]

# Préambule

Nous vivons actuellement une situation exceptionnelle.

La crise sanitaire due au COVID-19 a poussé les autorités à encourager très fortement le télétravail, ce qui implique dans l'immense majorité des cas l'usage d'outils numériques.

Mais, souvent, de mauvais réflexes (et reconnaissons le, de mauvaises interfaces) génèrent de la frustration et de l'agacement. Framasoft dispose d’une assez longue expérience et d'une double expertise (ce qui ne veut pas dire qu'on a réponse à tout !) :

1. Nous connaissons des outils, nous les mettons à disposition (et parfois nous les produisons)
2. Nous les pratiquons au quotidien (en tant qu'association de 35 bénévoles habitants 27 villes différentes, et une équipe de 9 salarié⋅es qui télétravaillent depuis les 5 coins de la France)

# Mise en garde

Ce document (volontairement) biaisé : les solutions présentées ne sont quasiment que des solutions libres. Cela à la fois parce qu'à Framasoft nous n'utilisons **que** des solutions libres, mais aussi (et surtout ?) parce que nous pensons que les solutions dites "propriétaires" (type Google, Microsoft, Facebook & co) sont toxiques. Cependant, au vu de la situation exceptionnelle ici, nous n'avons ni l'intention de porter un jugement sur les usages en temps de crise, ni la volonté de porter un discours politique qui nous tient pourtant à cœur. Nous présentons ces solutions tout simplement… parce que nous les utilisons !

Ce document est à destination de personnes ayant déjà des compétences de base ou intermédiaires en informatique. S'adresser au "grand public" débutant réclamerait un temps de rédaction que nous n'avons pour le moment pas. Et par ailleurs, nous pensons que s'adresser à "tous les publics" relève plutôt des missions de l'État.

Ce document est écrit dans l'urgence (le premier jet date du 13 mars 2019) et est donc nécessairement incomplet. Nous verrons à termes et suivant les besoins, si nous le complétons et l'enrichissons ou pas.

Ce document est sous licence libre (Creative Commons BY-SA) et donc améliorable, pour cela vous pouvez proposer vos améliorations sur le forum Framasoft[TODO PYG : lien] ou, si vous en avez les compétences, sur notre dépôt git..

# Avertissement spécifique Éducation Nationale

Le cadre scolaire est particulier.

Notamment, ci-dessous, nous ne proposons pas de solution pour de la visio-conférence prof => classe (ex 30 élèves). Ce qui est ballot, car PeerTube (notre alternative à Youtube) devrait permettre ça dans quelques mois (si tout va bien), mais pour l'instant nous finançons ce logiciel sur la base des dons à l'association avec 0,8 développeur, et qu'avec la crise en cours, le développement est fortement ralenti.

Globalement, si vous êtes enseignant⋅e, nous vous recommandons de faire pression sur votre hiérarchie pour qu'ils vous mettent à disposition le type d'outils présentés ci-dessous. On sait que ça n'est pas simple, mais les compétences existent en internes, et c'est vraiment la volonté politique qui manque.

A défaut vous pouvez utilisez les pads, les visio (petits groupes), les tchats. Mais, par pitié, de préférence chez des CHATONS. Car clairement, accueillir même 1% des 12 millions d'élèves/collégien⋅ne⋅s/lycéen⋅ne⋅s, c'est le boulot de votre Ministère et plus globalement de l'État, pas le nôtre.

# Rituels

## Début de journée

* Dire bonjour (tchat d'équipe).
* Prendre un café virtuel.
* Prendre 10mn pour évaluer la journée de la veille et celle à venir.
* Planifiez vos rdv audio de la journée.
* Fixez l'heure de votre fin de journée.
* Ouvrir les mails.
* Méthode GTD : répondez immédiatement à ce qui prends moins de 2mn. Classez le reste en fonction de ce qui prends plus de temps, et des urgences. (astuce Thunderbird)

## Pause

* toutes les 2H.
* 15/20mn de pause (mettez un minuteur, vous n'aurez plus à y penser).
* Levez-vous pendant votre pause

## Déjeuner

en télétravail, on a souvent tendance à manger (trop) vite.

Faites vous plaisir.

Coupez *vraiment* avec le travail.

## Fin de journée

* 5/10mn avant la fin de journée, prenez le temps de prendre quelques notes. Ce qui a été fait (reportez ce qui a été rayé de votre todolist), ce qui n'a pas fonctionné.
* Dites au revoir aux collègues sur le tchat.

Essayez de conserver un espace, une disposition physique spécifique à votre télétravail. Cela peut être d'utiliser telle chaise lorsque vous travaillez (et telle autre lorsque vous jouez sur votre ordi), ou bien fermer la porte de la salle, ou telle place sur le canapé... Mais le fait d'associer une position de son corps et une disposition de son outil au moment du travail, cela permet aussi de se couper avec les préoccupations du travail lorsque vous quittez cette disposition. Cela signifie aussi à votre entourage "si tu me vois comme ça : je bosse (donc ne me demande pas si les nouilles au frigo sont encore bonnes)".

**Autres sources :**

* <https://www.zdnet.fr/pratique/64-conseils-pour-survivre-en-teletravail-39900263.htm#Echobox=1584122087>

# Matériel

* Un ordinateur (fixe ou portable)
* Téléphone (un smartphone est un plus, mais pas indispensable).
* Un micro-casque (celui de votre téléphone peut très bien faire l'affaire)
* Connexion Internet : privilégiez au maximum le filaire !! (voir la partie "Économisez la bande passante")
* Une pièce au calme. Bureau et siège confortable. (TODO tuto/lien à trouver)
* Un minuteur. De préférence un physique (sinon vous allez jouer avec votre smartphone, on vous connaît !), celui de votre cuisine qui permet de savoir si les pâtes sont cuites fera très bien l'affaire). L'idée, c'est de pouvoir utiliser [la technique "Pomodoro"](https://www.ionos.fr/startupguide/productivite/technique-pomodoro/). C'est à dire des tâches de 15/25mn.
* Post-its, papiers brouillons, cahier, crayons. Recommandé : agenda papier.

# Logiciels sur l'ordinateur

Les cas sont évidemment bien trop variés pour les lister tous, donc nous allons nous contenter non seulement du strict minimum, mais en plus de ne pas proposer 1000 solutions.

## Indispensables

* Navigateur récent : Firefox (le seul dont le moteur n'est pas développé par une entreprise toxique)
* Suite Bureautique : LibreOffice

## Recommandés

* Logiciel de mail (Thunderbird). Oui, beaucoup de personnes utilisent Gmail. Sauf qu'en plus de vous surveiller, Gmail vous oblige à être connecté en permanence, etc. Tuto IMAP4. Thunderbird permet de travailler dans un environnement où votre attention est focalisée sur l'essentiel (= pas de twitter jusqu'à côté). (+ possibilité de travailler hors-ligne) Recommandé : Copier-coller multiples
* Client audio : Mumble (voir plus bas)

## Autres besoins ?

* [Framalibre](https://framalibre.org)
* [Alternativeto.net](https://alternativeto.net) (avec option "Free Software")

# Travailler à distance

## La base : des listes de discussion

Évidemment, nous recevons tous trop de mails, mais c'est un outil de base pour travailler à distance, car il constitue un "plus petit dénominateur commun" : quasiment tout le monde en a un, et la plupart des gens savent l'utiliser, il n'y a pas besoin de créer un compte.

Pour rappel, une liste de discussion, c'est une liste email (par exemple projet-bidule@framalistes.org) à laquelle Alice, Bob Carole et David peuvent s'abonner. Si Bob écrit un email à la liste, les 3 autres personnes abonnées recevront l'email et pourront y répondre. L'intérêt principal étant que l'on n'est alors plus obligé de se souvenir **qui** travaille sur le projet Bidule ou de leurs adresses, et qu'on peut ajouter des abonné⋅e⋅s ou en retirer facilement.

### Avantages

* Pas besoin de créer de compte
* Possibilité de créer plusieurs listes et de ne s'abonner qu'à celles qui vous intéressent
* Pas besoin d'expliquer comment fonctionne un mail
* Possibilité (souvent) d'accéder à l'historique des échanges
* Possibilité de créer des filtres emails pour trier par listes <https://support.mozilla.org/fr/kb/classer-vos-messages-en-utilisant-des-filtres>
* C'est de la communication asynchrone (vous répondez quand vous voulez/pouvez), et c'est donc reposant !

### Inconvénients

* Le volume de mails peut être important
* Peu de services de listes accessibles librement et gratuitement existent (et Framalistes est déjà débordé !). **AIDEZ NOUS EN PROPOSANT DES SERVICES DE LISTES DE DISCUSSIONS** (basé par exemple sur Sympa)
* C'est de la communication asynchrone (même si les mails arrivent généralement dans la minute, ils **peuvent mettre parfois 30mn à arriver**), et ça ne convient donc pas à des échanges rapides en mode "ping pong"

### Ressources

* [Framalistes](<https://framalistes.org>)
* [Sympa](<https://sympa.org>) (le logiciel qui motorise Framalistes) 
* 

## La messagerie téléphonique

Là aussi, on ne va vous en proposer qu'un (d'autres services existent) : Signal.

Il s'agit d'une messagerie de type Whatsapp / Télégram, mais libre (pas totalement, dirons les esprits chagrins, et ils auront bien raison), et correctement sécurisée (ce qui est sensé être le cas de Whatsapp et Telegram, mais dans les faits c'est impossible à vérifier).

### Avantages

* Possibilité de créer des groupes (avec des personnes ayant Signal)
* Messagerie et appels chiffrés (avec des personnes ayant Signal)
* possibilité de télécharger une application de bureau sur votre ordinateur (c'est plus rapide pour taper vos messages ;) )
* Notifications sur votre smartphone à réception du message (désactivable, heureusement !)

### Inconvénients

* Il faut avoir un smartphone
* Passe par le wifi/le forfait de données dès que l'autre est sur Signal aussi
* 

### Ressources

## Un cloud pour partager vos fichiers, agenda, contacts, visio

Le logiciel libre le plus répandu (et l'un des plus complets) est Nextcloud.

Le service Framadrive (qui repose sur Nextcloud) est plein depuis longtemps, nous envisagions de sortir un projet « Framacloud » prévu pour octobre, mais le planning risque d'être chamboulé. Heureusement, de nombreux membres du collectif CHATONS proposent du Nextcloud, souvent payant (qq euros par utilisateurs et par an, mais ça en vaut la peine !), parfois gratuit (chapeau bas à l'association La Mère Zaclys, qui en propose à des milliers d'utilisateur⋅ices, et propose même une offre gratuite aux écoles).

Evidémment, dans les structures suffisamment grandes, vous pouvez parfaitement demander à vos responsables informatique de l'installer. Si les besoins de collaborer en tant réel sur des documents structurés sont forts, cherchez des offres proposant une suite bureautique intégrée (Collabora Online ou OnlyOffice), ou demandez à votre responsable informatique d'ajouter l'application.

Le fonctionnement de Nextcloud est *relativement* simple, mais le logiciel est foisonnant d'options et de plugins/applications. Nous reviendrons peut-être plus tard sur son usage avancé, mais beaucoup de documentation existe déjà en ligne, alors on vous laisse chercher !

### Avantages

* Interface relativement simple


* « Couteau suisse » du cloud : propose la gestion des fichiers, mais aussi agenda, gestion des contacts, synchronisation avec le téléphone, gestion (basique) de projets, etc.


* Solution très répandue, donc beaucoup de support disponible en ligne.

### Inconvénients 

* Un peu d'accompagnement est le bienvenu pour commencer, car les options sont nombreuses
* Si vous avez beaucoup d'utilisateur⋅ices en ligne au même moment, Nextcloud n'est pas un foudre de guerre et peut ramer.

### Ressources

## La synchronisation des fichiers

L'un des gros avantages de Nexcloud, c'est qu'il dispose d'un « client de synchronisation ». Le principe est sensiblement le même qu'avec DropBox, par exemple : tout ce que vous déposerez dans le dossier /Nextcloud/ sera automatiquement envoyé en ligne. Et tout dossier ou document créé en ligne, sera automatiquement renvoyé en local sur votre machine. Pas mal, non ?

Mais c'est encore mieux quand on partage des dossiers avec les collègues. 

Par exemple si Alice modifie le fichier "budget prévisionnel" (avec LibreOffice Calc, l'alternative libre à Excel, par exemple) sur son ordinateur, depuis un dossier suivi par le client Nextcloud, il sera **automatiquement envoyé en ligne** lors de sa sauvegarde. Et il devient automatiquement disponible pour Carole et Bob, qui peuvent le modifier aussi (si je Alice leur en a donné le droit). Cela implique un peu de coordination, et notamment le fait qu'on s'organise avec les collègues en amont pour différencier les fichiers "froids" (qui sont peu modifiés) et les fichiers "chauds" (modifiés plusieurs fois par jour ou même par heure). Pour les fichiers "chauds", privilégier les pads ou la suite bureautique en ligne.

### Avantages 

On a pas a se précoccuper d'envoyer les fichiers en ligne : tout ce qui se trouve dans le dossier "Nextcloud" de son ordinateur est automatiquement synchronisé en ligne.

L'inverse est vraie aussi : le client Nextcloud vérifie que votre fichier en local sur votre ordinateur a bien la dernière version disponible. Si une version + récente est déposée en ligne, elle sera automatiquement téléchargée sur votre machine (écrasant votre version)

### Inconvénients

* risque de supprimer des fichiers par inadvertance
* déterminer quel compte héberge les fichiers communs (partagés avec les autres comptes)
* Il faut installer et configurer un logiciel sur son ordinateur
* pour la synchronisation, il faut (évidemment) une connexion à internet.

### Ressources

## Les documents collaboratifs (pads)

Encore relativement méconnus, ils vous changeront la vie ! 

Il s'agit de page web qui vous permettent de rédiger un texte en ligne, seul (prise de notes) ou à plusieurs (simultanément) 

Les différences avec une suite bureautique en ligne ? Les pads sont + légers, + "immédiats", "compostables" (ils peuvent s'autodétruire après une période définie sans modification), ils donnent une vision claire de "qui modifie quoi ?". Usages : brainstorming, prise de notes en réunion. Chaque pad dispose de son tchat. La sauvegarde se fait automatiquement… à chaque frappe sur le clavier. Enfin, il est possible de "rembobiner" un pad dans le temps.

Certains sites proposant des pads proposent aussi un plugin "MyPads" (développé par Framasoft, mais que chacun⋅e peut installer) qui permet de gérer des pads publics, restreints par mot de passe, ou privés (via la création de comptes et de groupes). MyPads permet aussi de gérer vos pads par dossiers (ce qui est bien pratique).

### Avantages

* pas d’inscription (sauf MyPads) pas d'installation, il suffit d'avoir l'adresse pour travailler en direct
* L'historique des modifications enregistre tout (même le "merde au prof" tapé lorsque le dos est tourné et vite effacé...)
* On peut travailler à plusieurs dizaines en même temps sur un même document
* Pas besoin de sauvegarder :)

### Inconvénients

* Si pas de MyPads (dossiers), n'importe qui peut venir voir voter travail pourvu qu'il ou elle trouve l'URL
* Utilisation mobile limitée (lecture OK, écriture bof)
* Il faut bien penser à noter l'URL de son pad, sous peine de l'oublier (et de ne jamais le retrouver ;) )
* Les options de mise en forme sont limitées (gras, italiques, souligné, titres, etc. Mais pas de tableau ou d'images dans la plupart des cas)

### Où trouver un tel service ?

* Framapad.org :
* mypads.framapad.org : possibilité
* chatons : liste

## Le tchat d'équipe

Successeur du vénérable IRC (encore très utilisé dans certains milieux). Objectif : dialogue en temps réel par "canaux de discussions" (ex: "Projet Bidule", "Discussions budget", "Veille partagée", "Machine à café", etc). Le principe est simple : vous créez une équipe (par exemple "Association Machin"), vous invitez les personnes concernées. Puis chaque personne peut alors soit rejoindre un canal public (ex: "Machine à café") qui seront accessibles par tous, ou créer des canaux privés (ex : "Projet Bidule") dans lequel le créateur du canal pourra inviter uniquement les membres concernés. Ensuite, et bien… les personnes discutent entre elles :)

Fil de discussion organisé. Possibilité de mentionner des utilisateurs. Ex: si utilisateur "michel", écrire "@michel" dans un message permet de prévenir Michel (il recevra un mail avec le message, et pourra se connecter pour répondre en ligne, ou s'il est déjà connecté, il recevra une notification à l'écran). Si l'application Mattermost (Android/Iphone) est installée et configurée sur son portable, il recevra une notification sur son smartphone (et pourra répondre depuis son smartphone).

Possible d'ajouter des fichiers, mais pas forcément recommandé (passer par un un logiciel de dépôt de fichier/image temporaire, puis copier-coller le lien)

### Avantages

* Discussion « temps réel »
* 

### Inconvénients 

* Risque d'être noyé dans l'information
* 

### Ressources

* [Framateam](https://framateam.org/)
* [Riot/Matrix](<https://about.riot.im/>)

## Partage de fichiers ou d'images

Cas d'usage : Alice veut envoyer montrer un fichier vidéo à Bob, mais celui-ci pèse 50Mo et ne passe pas par email. => Framadrop ou équivalent. Cas d'usage : Bob veut partager 5 propositions de logos à Alice et Jean. => Framapic ou équivalent.

### Avantages 

* Facile à transmettre

### Inconvénients 

* Les fichiers ont un temps d'expiration défini
* Nécessite de garder trace des liens

### Où trouver un tel service ?

# Prendre des décisions

Cas d'usage : Alice souhaite que son équipe valide sa vidéo de présentation de l'entreprise. En présentiel, elle aurait organisé une réunion avec les 5 membres de l'équipe, aurait bloqué une heure sur leur planning, puis le jour J à l'heure H, tout ce petit monde aurait débattu, fait remonter ses remarques, et validé (ou pas) la vidéo.

A distance, le processus est sensiblement le même, mais comme les réunions sont à éviter (cf plus bas), le processus peut être allégé de différentes manières. Par exemple, à Framasoft, le fonctionnement général est le suivant (il peut y avoir des exceptions, l'essentiel étant de faire preuve de bon sens pour s'adapter aux situations) :

* Alice va d'abord "préparer le terrain" pour ces collègues. L'idée, c'est de leur faciliter la vie pour que la décision puisse être rapide.
  * Elle va mettre en ligne la vidéo (ex: Framadrop, durée de vie d'une semaine) et copier son URL.
  * Puis elle va ouvrir un pad (durée de vie d'une semaine aussi), y coller l'adresse de la vidéo et préparer quelques questions et points clés auxquels ses collègues pourront répondre (par exemple "La vidéo est-elle trop longue ?", "Quel est votre sentiment général ?", etc. en laissant quelques lignes entre chaque points pour que ces collègues puissent y répondre.
  * Enfin, elle va se rendre sur le tchat d'équipe, dans le canal "Projet bidule", et poster une message du type : "Salut @channel : pourriez-vous vous rendre sur le framapad \\url{https://hebdo.framapad.org/XXXXXXXXXX} quand vous pourrez. C'est pour me faire un retour sur la vidéo du projet Bidule. J'aurai besoin de vos retours avant jeudi prochain ?"
* les collègues d'Alice vont tous et toutes recevoir une notification (grâce au "@channel" dans son message), que ce soit à l'écran, par téléphone ou par email.
* ils pourront aller sur le pad, et télécharger la vidéo
* ils pourront répondre aux questions d'Alice sur le pad
* puis ils pourront retourner sur le tchat, et répondre (par exemple) "OK, c'est vu pour moi" ou "J'ai répondu sur le pad, mais j'ai un problème compliqué avec un point spécifique, on pourrait s'appeler pour en parler ?"
* Alice aura donc récupéré les retours de tous ses collègues, et le pad (hebdo) s'effacera automatiquement au bout d'une semaine sans modification, ce qui est largement suffisant (au pire, elle peut exporter son contenu en .pdf, .doc, .odt ou .txt). Idem pour la vidéo, qui s'effacera au bout d'une semaine (et n'encombrera pas de serveur).

Bon, ça c'est le cas où tout se passe bien, mais parfois, certaines décisions sont plus compliquées à prendre.

Dans ce cas là, on peut passer à une étape où on va demander à chacun de préciser sa position.

Par exemple, si la vidéo d'Alice enthousiasme certaines personnes, alors que d'autres sous-entendent la détester, et que 2 collègues ne se sont pas exprimés, Alice a plusieurs solutions :

Solution 1. Faire un vote (ce n'est pas une élection, ou un jugement sur le travail général d'Alice, mais juste un outil qui lui permettra de "lire" plus facilement la position de ses collègues) Alice peut créer un sondage avec l'outil Framadate et inviter ses collègues à y répondre de façon tranchée (ex: "Cette vidéo vous semble-t-elle bien représenter votre travail au sein de la structure ?" Oui / Non. Puis poster l'adresse du sondage sur le tchat d'équipe. Alice recevra un mail à chaque vote (et pourra relancer les retardataires).

Solution 2. Lancer une recherche de consensus. Le tchat d'équipe ne suffit pas ? Les discussions par email sont interminables ? Alice peut passer à l'étape supérieure en ouvrant une discussion sur un outil comme Framavox. (Inconvénient, il faudra que chacun ait un compte sur cet outil). Ce genre d'outil est précieux pour essayer de trouver un consensus sur des problèmes complexe en essayant de "découper" ce problème en plusieurs sous-problèmes, puis en invitant chacun à se positionner dessus (au départ par texte, puis par différente modalités de "vote" où chacun peut exprimer ses choix)

Solution 3. La réunion à distance. Voir ci-dessous.

# Se réunir à distance

Que celles et ceux qui aiment les réunions lèvent la main ! Attendez, on vous compte... Bon, vous êtes 4,2%, et comme par hasard, c'est vous qui provoquez 80% des réunions.

Sérieusement, la réunion à distance, ça doit être le dernier recours. Non seulement parce que c'est parfois complexe à organiser, mais aussi parce que le télétravail, ça n'est pas juste "le bureau à la maison". C'est une organisation différente du travail en présentiel. Peut être que Bob ira chercher ses enfants à 16h, alors qu'Alice a rdv à 18h. Dans ces conditions, est-il vraiment indispensable d'organiser cette réunion à 17h ? **Détendez-vous et faites confiance à vos collègues.** Dans une immense majorité de cas, les réunions "live" ne servent à … presque rien ! Elles peuvent être remplacées par des prises de décisions spécifiques successives (voir plus haut).

Les 3 cas où les réunions "live" nous semblent importantes sont :

1. une prise de connaissance 
Par exemple la structure X veut vous présenter son projet. Vous avez préalablement demandé à la structure X de vous faire un mail pour vous présenter les choses dans les grandes lignes. A la lecture du mail, vous êtes intéressé. Du coup, vous convenez d'un appel téléphonique ou d'une visio à 2.
2. Les transmissions d'informations
Il s'agit, typiquement, des réunions d'équipe. Le rythme ne doit pas être trop important (pas la peine de mobiliser 6 personnes tous les jours ! Si vous êtes transparents sur le tchat d'équipe, chacun saura ce qu'ont fait les autres (ou pourra poser la question quelques jours plus tard), ni trop faible (une réunion d'équipe par mois, c'est la meilleure façon de laisser les personnes "en plan" sans qu'elle puissent exprimer leurs difficultés, leurs satisfaction s, leurs frustrations, etc.). Une réunion par semaine semble un bon rythme, mais à vous de le trouver !
3. Les dissensus inextricables
Toute organisation rencontre de temps à autres des situations (souvent liées au Putain de Facteur Humain) qui n'ont pas trouvé de solution par les processus de décisions classiques. Dans ce cadre là, organiser une réunion (et bien la préparer en amont !) peut être une solution.

## Réunion audio / audio conférence

### Cas #1 - Moins de 10 : Bon vieux téléphone

#### Avantages

* Le téléphone, tout le monde sait comment ça marche
* Pas de mise en place technique compliquée
* 

#### Inconvénients

* Avertissement : lire "préparer et réussir une réunion à distance"
* Coût d'une communication téléphonique standard, c'est à dire le plus souvent gratuit à l'intérieur du pays, mais peut être payant si des participants viennent d'autres pays
* 

#### Ressources

* https://ovh.com/conferences
* (sans doute d'autres)
* Appel classique multi-participants ? Supporté sur certains téléphones ?
* 

### Cas #2 - plus de 10 : Mumble

Déjà, c'est probablement une mauvaise idée d'avoir plus de 10 participants. Si c'est pour prendre une décision, faites un sondage ou utilisez des outils de recherche de consensus.

Alternative à Discord ou TeamSpeak Attention, ce n'est pas un logiciel en ligne : il faut que chaque participant installe le logiciel avant. Pour un bon fonctionnement de la réunion, il est fortement recommandé d'avoir pris le temps de configurer Mumble selon vos besoins et d'avoir fait des tests en amont avec vos participant⋅e⋅s.

Important : utiliser de préférence le "push to talk" (votre micro est automatiquement fermé, sauf lorsque vous appuyez sur une touche prédéfinie).

#### Avantages

* Peut fonctionner avec un nombre très important de participant⋅e⋅s (100 et +)
* Bonne qualité audio
* Possibilité d'enregistrer la conversation (prévenez vos interlocuteurs en amont)
* Application Android "Plumble" / Application iPhone disponible aussi. Cela ne dispense pas d'activer le mode "push to talk" ou de couper son micro.
* 

#### Inconvénients

* Avertissement : lire "préparer et réussir une réunion à distance"
* La configuration peut parfois être compliquée pour des utilisateurs peu habitués au numérique (l'interface est vieillote)
* Il n'est pas

#### Où trouver un tel service ?

* Serveur Mumble de l'APRIL : https://wiki.april.org/w/Mumble
* (sans doute d'autres)
* 

# Réunion vidéo / Visio conférence

TODO

![La visioconférence habituelle](https://pbs.twimg.com/media/ETPL1kjWAAEwzUI?format=png)

# Préparer (et réussir) une réunion à distance

## Rappel sur le nombre de participant⋅e⋅s

Plus de 10 ? Cette réunion "live" est sans doute une mauvaise idée ! Associations : sachez qu'il est parfaitement possible, si vos statuts le permettent, d'organiser des A.G. en ligne, sans visio ! A Framasoft, nous pratiquons régulièrement ce type d'AG à distance, en passant par notre tchat d'équipe, et où les 35 membres de l'association s'expriment à l'écrit uniquement. Et ça marche très bien, à condition d'avoir bien préparé les choses en amont.

## Une réunion ça se prépare !

Enfonçons une porte ouverte, mais une réunion sans Ordre Du Jour est vouée à l'échec. Quitte à ce que cet ordre du jour change en cours de réunion (ce n'est pas interdit !), mais vos interlocuteurs doivent savoir ce qu'ils font là, et doivent avoir un "guide" sous les yeux. Minuter l'ODJ est super important

Une bonne façon de préparer en amont :

* désigner un⋅e "facilitateur⋅ice" (souvent vous) qui se chargera : des invitations, des tests préalables (voir ci-dessous), de l'accueil des participants, de s'assurer que chacun est à l'aise avec les outils, du respect des horaires.
* désigner un⋅e "animateur⋅icec" qui veillera au respect de l'horaire et du suivi de l'ordre du jour (Chez nous on a : un.e garant.e de l'ordre du jour (qui vérifie qu'on ne s’éloigne pas du sujet), un.e ou plusieurs scriptes pour les notes (dont une personne qui s'assure que les notes sont exportées ou sauvegardées) , un.e dispatcheur qui distribue la parole ou la donne à celle et ceux qui ne la prennent pas spontanément, ce qui est encore plus important que d'habitude à distance, un.e garant.e du temps). Je vous colle en bas du pad le début notre pad "réunion d'équipe"
* lancer les invitations (par exemple avec un framadate) au moins 15j avant
* ouvrir un pad spécifique à la réunion
* lister sur ce pad la date, la liste des participants (que chacun devra compléter, ne serait-ce que pour leur rappeler qu'ils peuvent agir sur le pad), les heures de début et de fin (prévues et réelles) de la réunion, l'ordre du jour, les adresses internet qui pourraient être utile.

### Les points à vérifier en amont

Assurez vous d'**être au calme** (de préférence dans un bureau fermé plutôt que dans un café !) et de ne pas être dérangé. Coupez la sonnerie de votre téléphone. N'ouvrez *que* les logiciels et pages web nécessaires (= fermez Twitter !).

**Il faut ABSOLUMENT tester votre matériel avant.** Si vous donnez rendez-vous à 14H sur Mumble, et que vous installez le logiciel à l'arrache à 13h58, c'est perdu : à 14h15, vous serez encore à essayer de configurer votre micro. N'hésitez pas à tester plusieurs jours avant. Une fois que vous serez rodés, vous pourrez vous connecter 2 mn avant. Mais si vous débutez, exercez-vous ! (par exemple avec des collègues ou amis).

**Chaque participant doit être *fortement invité* à tester son matériel avant.** Vous n'inviteriez pas des gens venant de toute la France à une réunion en leur donnant l'adresse du rdv 5mn avant le début ? Et bien ne faites pas la même chose en ligne ! Par exemple, si votre réunion a lieu sur Mumble, proposez aux particpants, plusieurs jours avant (!), de les accompagner dans l'installation et la configuration du logiciel. Une fois qu'ils auront fait 3 ou 4 réunions, cela ne sera plus nécessaire. Mais si vous ne le faites pas, underlinesoyez assuré que les 45 premières minutes de réunions seront passées à aider Gérard à configurer sa carte sonunderline. Ce qui créera de la frustration.

Vous utilisez de l'audio (ou de la vidéo) ?\*\* Imposez le micro-casque !\*\* La plupart des logiciels (voir la plupart des micros et carte son) disposent de dispositifs de réduction d'écho et de réduction de bruits ambiants. Mais leur qualité est extrêmement variable ! Du coup, c'est le larsen assuré, ou la forte probabilité d'entendre votre propre écho lorsque que vous parlez, tout simplement parce que votre douce voix sortira sur les enceintes de Bob, et que son micro (pensant que c'est Bob qui parle) vous la renverra en écho. J'insiste : imposez le micro-casque à chaque interlocuteur. Tout le monde dispose de ce genre de casque, vendu d'office avec les téléphones. Pas besoin d'investir dans de la haute technologie, le moindre micro-casque à 10€ fera très bien l'affaire, et est déjà conçu pour réduire les bruits ambiants, et évite les problèmes de larsen.

En début de réunion, **faites une "météo" des personnes**, en les invitant chacun à leur tour, à se présenter et/ou à s'exprimer en quelques mots sur comment ils vont. L'objectif est quadruple :

1. s'assurer que chacun est dans de bonnes conditions pour la réunion (ou comprendre pourquoi ça ne sera peut être pas le cas). Par exemple, si Bob vient d'apprendre une heure auparavant qu'il devait garder son enfant malade, il sera probablement préoccupé et pas forcément concentré, mais au moins vous saurez pourquoi.
2. le non verbal, c'est important, mais en audioconf, c'est … impossible ! (et en visio c'est à peine mieux). Pendant que chacun se présente, vous pouvez repérer des intonations de voix différentes, qui pourront blablabla
3. parfois, tout le monde ne se connaît pas, et faire un rapide tour de table permet à chacun d'identifier les voix des personnes présentes. Bien de doubler ce tour de table sonore avec le pad (chaque personne écrit son prénom, nom, etc sur le pad, ça permet aux nouveaux arrivant.e.s de voir qui est présent sans que l'animateur.trice doive refaire la liste , et ça permet aux participant.e.s de se familiariser avec le pad, de voir la magie d'écrire en direct et de voir écrire les autres ;)).
4. cela permet enfin de vérifier que les outils fonctionnent correctement (les téléphones, les micros, les pads, etc)

Après ce tour de table, invitez le facilitateur à rappeler les règles, même succintement : on ne se coupe pas la parole, heure de fin, adresse du pad, le fait que la prise de note est collaborative, etc.

**N'ayez pas peur de "kicker" quelqu'un !** Bob ne joue pas le jeu et se pointe à la réunion sans avoir testé son matériel ? En conséquence, il fait perdre 10 mn à 10 autre persones et, en plus, émet des bruits bizarroides en permanence empêchant d'être concentré sur la conversation ? Alors, virez Bob ! Désolé pour Bob, mais dans une réunion physique on n'arrive pas avec des enceintes jouant de la musique forte. Bob n'est pas prêt ? Tant pis. Demandez lui de partir et de revenir quand il aura fait en sorte de régler ses problèmes. (méthode avec Mumble et Jitsi)

**Ne parlez pas tous en même temps ! ……** Un autre calvaire des réunions à distance : les gens qui se coupent la parole et qu'on ne reconnaît même pas, les gens qui toussent, les klaxons de la rue qui vont être entendus par tout le monde. Le principe : l'animateur⋅ice, en début de réunion (et pendant si nécessaire) rappelle que les personne qui ne prennent pas la parole doivent couper leur micro. Et que les personnes qui prennent la parole doivent redire leur prénom avant chaque intervention.

**Sur la plupart des téléphones**, il y a souvent une fonctionnalité pour cela (le plus souvent située à côté du mode "haut-parleur"). **Sur Mumble**, vous pouvez utiliser le mode "push to talk" : vous pouvez configurer une touche (ou une combinaison de touches) de votre clavier pour activer ce mode de communication. Tant que cette/ces touches ne sont pas appuyées, vous n'émettez aucun son (vous pouvez donc renifler tranquillement ;-) ). Lorsque vous pressez cette touche, votre micro s'ouvre, et votre voix sera transmise à tout⋅e⋅s les participant⋅e⋅s du salon Mumble. **Sur Jitsi/Framatalk**, vous avez une option en bas de l'écran qui vous permet de désactiver ou réactiver votre micro d'un simple clic. Pour demander la parole, vous pouvez cliquez sur l'icône "Main" qui signalera à tout le monde que vous souhaitez vous exprimez.

Nous sommes conscient⋅e⋅s que cela n'est pas simple lors des premières prises de paroles, et qu'il y a un coup à prendre, mais cela vaut vraiment la peine de s'adapter à ces nouvelles pratiques.

…… Mais veillez à ce que chacun⋅e puisse s'exprimer Comme les interlocuteur⋅ice⋅s ne se voient pas, il faut là aussi faire preuve de discipline. Chacun⋅e, en fin de prise de parole, doit laisser quelques secondes a ses interlocuteurs pour s'exprimer (notamment pour qu'ils et elles puissent avoir le temps de réactiver leur micro).

# Trucs et astuces

## Économisez la bande passante !

Chez vous, vous voulez travailler en musique ? OK, mais pas Spotify/Deezr/Youtube => Radio ou téléchargement Netflix => ressortez vos DVD (faites une bourse d'échanges dans l'immeuble)... Lisez ! Éviter de garder Facebook ouvert ?

<https://www.arcep.fr/demarches-et-services/utilisateurs/teletravail-et-connexion-internet.html>

#### Crédits & licence de ce document

Ce document est publié sous licence Creative Commons by-sa

##### Auteurs et autrices : 

* Équipe Framasoft (principalement pyg, mais améliorations de Pouhiou, Numahell, et bien d'autres)
* 
